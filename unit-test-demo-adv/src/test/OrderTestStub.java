package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import domain.Order;
import domain.Warehouse;

public class OrderTestStub {

	private static final String JUICE = "���"; 
	private static final String BEER = "����"; 
	
	class WarehouseStub extends Warehouse {

		@Override
		public void add(String product, int quantity) {
		}

		@Override
		public void remove(String product, int quantity) {
		}

		@Override
		public int getInventory(String product) {
			return 100;
		}

		@Override
		public boolean hasInventory(String product, int quantity) {
			if (quantity <= 100)
				return true;
			else
				return false;
		}
	}
	
	
	
	private Warehouse warehouse;
	
	@Before
	public void setUp() {
		warehouse = new WarehouseStub();
//		warehouse.add(BEER, 100);
//		warehouse.add(JUICE, 50);
	}
	
	@Test
	public void testOrderWithEnoughGoods() {
		Order order = new Order(BEER, 40);
		order.fill(warehouse);
		assertTrue(order.isFilled());
//		assertEquals(60, warehouse.getInventory(BEER));
	}
	
	@Test
	public void testOrderWhenNotEnoughGoods() {
		Order order = new Order(BEER, 101);
		order.fill(warehouse);
		assertFalse(order.isFilled());
//		assertEquals(100, warehouse.getInventory(BEER));
	}
	
}
