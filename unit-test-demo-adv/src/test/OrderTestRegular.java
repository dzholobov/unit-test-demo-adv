package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import domain.Order;
import domain.Warehouse;

public class OrderTestRegular {

	private static final String JUICE = "���"; 
	private static final String BEER = "����"; 
	
	private Warehouse warehouse;
	
	@Before
	public void setUp() {
		warehouse = new Warehouse();
		warehouse.add(BEER, 100);
		warehouse.add(JUICE, 50);
	}
	
	@Test
	public void testOrderWithEnoughGoods() {
		Order order = new Order(BEER, 40);
		order.fill(warehouse);
		assertTrue(order.isFilled());
		assertEquals(60, warehouse.getInventory(BEER));
	}
	
	@Test
	public void testOrderWhenNotEnoughGoods() {
		Order order = new Order(BEER, 101);
		order.fill(warehouse);
		assertFalse(order.isFilled());
		assertEquals(100, warehouse.getInventory(BEER));
	}
	
}
