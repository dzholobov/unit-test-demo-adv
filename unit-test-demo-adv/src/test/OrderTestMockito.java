package test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;

import domain.Order;
import domain.Warehouse;

public class OrderTestMockito {

	private static final String JUICE = "���"; 
	private static final String BEER = "����"; 
	
	private Warehouse warehouse;
	
	@Before
	public void setUp() {
		warehouse = mock(Warehouse.class);
		when(warehouse.getInventory(BEER)).thenReturn(100);
		when(warehouse.hasInventory(BEER, 40)).thenReturn(true);
		when(warehouse.hasInventory(BEER, 101)).thenReturn(false);
	}
	
	@Test
	public void testOrderWithEnoughGoods() {
		Order order = new Order(BEER, 40);
		order.fill(warehouse);
		
		assertTrue(order.isFilled());
		
		verify(warehouse).hasInventory(BEER, 40);
		verify(warehouse).remove(BEER, 40);
	}
	
	@Test
	public void testOrderWhenNotEnoughGoods() {
		Order order = new Order(BEER, 101);
		order.fill(warehouse);

		assertFalse(order.isFilled());
		
		verify(warehouse).hasInventory(BEER, 101);
		verify(warehouse, never()).remove(anyString(), anyInt());
	}
	
}
