package dummy;

public class Address {

	private int postalCode;
	private String streetAddress;

	public Address(int postalCode, String streetAddress) {
		this.postalCode = postalCode;
		this.streetAddress = streetAddress;
	}

	public int getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(int postalCode) {
		this.postalCode = postalCode;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

}
