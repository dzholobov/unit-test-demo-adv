package dummy;

public class Country {

	private int phoneCode;
	private String name;

	public Country(int phoneCode, String name) {
		super();
		this.phoneCode = phoneCode;
		this.name = name;
	}

	public int getPhoneCode() {
		return phoneCode;
	}

	public void setPhoneCode(int phoneCode) {
		this.phoneCode = phoneCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
