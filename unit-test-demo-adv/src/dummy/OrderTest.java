package dummy;

import static org.junit.Assert.*;

import org.junit.Test;

public class OrderTest {

	class DummyCustomer extends Customer {
		DummyCustomer() {
			super("", null, null, null);
		}
	}

	
	@Test
	public void testTotal() {
		Country country = new Country(7, "��");
		City city = new City(30, "���������");
		Address address = new Address(414056, "��. ��������, 20");
		Customer customer = new Customer("���� ������", address, city, country);
		
		Order order = new Order(customer);
		order.addItem("�����������", 2000, 1);
		order.addItem("iPad", 300, 2);
		assertEquals(2600, order.total());
	}

	@Test
	public void testTotalWithDummy() {

		Customer customer = new DummyCustomer();
		
		Order order = new Order(customer);
		order.addItem("�����������", 2000, 1);
		order.addItem("iPad", 300, 2);
		assertEquals(2600, order.total());
	}
	
	
}
