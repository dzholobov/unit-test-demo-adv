package dummy;

public class City {

	private int regionCode;
	private String name;

	public City(int regionCode, String name) {
		this.regionCode = regionCode;
		this.name = name;
	}

	public int getRegionCode() {
		return regionCode;
	}

	public void setRegionCode(int regionCode) {
		this.regionCode = regionCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
