package domain;

import java.util.HashMap;
import java.util.Map;

public class Warehouse {

	private Map<String, Integer> products = new HashMap<>();
	
	private Logger logger = new Logger();
	
	public void add(String product, int quantity) {
		int currentQuantity = getInventory(product);
		products.put(product, currentQuantity + quantity);
//		logger.log("���������: " + product + ", ����������: " + quantity);
	}
	
	public void remove(String product, int quantity) {
		int currentQuantity = getInventory(product);
		if (currentQuantity - quantity >= 0) {
			products.put(product, currentQuantity - quantity);
//			logger.log("�������: " + product + ", ����������: " + quantity);
		} else {
//			logger.log("��������� ������� ��������: " + product + 
//					", ����������: " + quantity + ", �� ������ " + currentQuantity);
		}
	}
	
	public int getInventory(String product) {
		return products.getOrDefault(product, 0);
	}
	
	public boolean hasInventory(String product, int quantity) {
		return getInventory(product) >= quantity;
	}
}
