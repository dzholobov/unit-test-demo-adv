package domain;

import java.io.FileWriter;
import java.io.IOException;

public class Logger {

	private String logFile;
	
	public Logger() {
		logFile = "warehouse.log";
	}
	
	public void log(String mesage) {
		try {
			FileWriter fw = new FileWriter(logFile, true);
			fw.write(mesage + System.lineSeparator());
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
