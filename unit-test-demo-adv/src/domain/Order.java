package domain;

public class Order {

	private String product;
	private int quantity;
	private boolean filled = false;
	
	public Order(String product, int quantity) {
		super();
		this.product = product;
		this.quantity = quantity;
	}

	public String getProduct() {
		return product;
	}

	public int getQuantity() {
		return quantity;
	}

	public void fill(Warehouse warehouse) {
		if (warehouse.hasInventory(product, quantity)) {
			warehouse.remove(product, quantity);
			filled = true;
		}
	}
	
	public boolean isFilled() {
		return filled;
	}

}
